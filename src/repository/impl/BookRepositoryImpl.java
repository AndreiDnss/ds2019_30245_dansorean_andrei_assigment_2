package repository.impl;

import model.Book;
import repository.BookRepository;

import java.sql.*;
import java.util.*;
import model.*;
import java.util.List;

public class BookRepositoryImpl implements BookRepository {



    private final JDBConnectionWrapper jdbConnectionWrapper;


    //  private static final Logger LOGGER = Logger.getLogger(ActivitiesRepositoryDecorator.class.getName());

    // private final ActivitiesRepository activitiesRepository;


    public BookRepositoryImpl(JDBConnectionWrapper jdbConnectionWrapper) {
        this.jdbConnectionWrapper = jdbConnectionWrapper;
        //  this.activitiesRepository = activitiesRepository;
    }


    @Override
    public Book create(Book book) {

        Connection connection = jdbConnectionWrapper.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO book (id,title, author, genre, quantity, price) VALUES(?,?, ?, ?, ?, ?)",
                    Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, book.getId());
            preparedStatement.setString(2, book.getTitle());
            preparedStatement.setString(3, book.getAutor());
            preparedStatement.setString(4, book.getGen());
            preparedStatement.setInt(5, book.getQuantity());
            preparedStatement.setInt(6, book.getPrice());

            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();

            if (resultSet.next()) {
                book.setId(resultSet.getInt(1));
                return book;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;

    }

    @Override
    public Book update(Book book) {
        Connection connection = jdbConnectionWrapper.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE book SET title=?, author=?, genre=?, quantity=?, price=? WHERE id=?",
                    Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, book.getTitle());
            preparedStatement.setString(2, book.getAutor());
            preparedStatement.setString(3, book.getGen());
            preparedStatement.setInt(4, book.getQuantity());
            preparedStatement.setInt(5, book.getPrice());
            preparedStatement.setLong(6, book.getId());

            int changedRows = preparedStatement.executeUpdate();

            if(changedRows > 0) {
                return book;
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean delete(int id) {

        Connection connection = jdbConnectionWrapper.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM book WHERE id= ? ");
            preparedStatement.setLong(1, id);


            int updatedRows = preparedStatement.executeUpdate();

            return updatedRows > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<Book> view() {
        Connection connection = jdbConnectionWrapper.getConnection();
        List<Book> books = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM book");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Book book = new Book();

                book.setId(resultSet.getInt(1));
                book.setTitle(resultSet.getString(2));
                book.setAutor(resultSet.getString(3));
                book.setGen(resultSet.getString(4));
                book.setQuantity(resultSet.getInt(5));
                book.setPrice(resultSet.getInt(6));

                books.add(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return books;
    }

    @Override
    public Book findById(int id) {
        Connection connection = jdbConnectionWrapper.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM book WHERE id=?");
            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                Book book = new Book();

                book.setId(resultSet.getInt(1));
                book.setTitle(resultSet.getString(2));
                book.setAutor(resultSet.getString(3));
                book.setGen(resultSet.getString(4));
                book.setQuantity(resultSet.getInt(5));
                book.setPrice(resultSet.getInt(6));

                return book;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public List<Book> findByTitle(String title) {
        Connection connection = jdbConnectionWrapper.getConnection();
        List<Book> books = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT * FROM book " +
                            "WHERE title=?");
            preparedStatement.setString(1, title);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Book book = new Book();
                book.setId(resultSet.getInt(1));
                book.setTitle(resultSet.getString(2));
                book.setAutor(resultSet.getString(3));
                book.setGen(resultSet.getString(4));
                book.setQuantity(resultSet.getInt(5));
                book.setPrice(resultSet.getInt(6));

                books.add(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return books;
    }

    @Override
    public List<Book> findByAuthor(String autor) {
        Connection connection = jdbConnectionWrapper.getConnection();
        List<Book> books = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT * FROM book " +
                            "WHERE author=?");
            preparedStatement.setString(1, autor);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Book book = new Book();
                book.setId(resultSet.getInt(1));
                book.setTitle(resultSet.getString(2));
                book.setAutor(resultSet.getString(3));
                book.setGen(resultSet.getString(4));
                book.setQuantity(resultSet.getInt(5));
                book.setPrice(resultSet.getInt(6));

                books.add(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return books;    }

    @Override
    public List<Book> finfByGenre(String genre) {
        Connection connection = jdbConnectionWrapper.getConnection();
        List<Book> books = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT * FROM book " +
                            "WHERE genre=?");
            preparedStatement.setString(1, genre);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Book book = new Book();
                book.setId(resultSet.getInt(1));
                book.setTitle(resultSet.getString(2));
                book.setAutor(resultSet.getString(3));
                book.setGen(resultSet.getString(4));
                book.setQuantity(resultSet.getInt(5));
                book.setPrice(resultSet.getInt(6));

                books.add(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return books;
    }

    @Override
    public List<Book> findByTitleAndAuthor(String title, String author){

        Connection connection = jdbConnectionWrapper.getConnection();
        List<Book> books = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM book WHERE title=? AND author=?");
            preparedStatement.setString(1, title);
            preparedStatement.setString(2, author);


            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                Book book = new Book();

                book.setId(resultSet.getInt(1));
                book.setTitle(resultSet.getString(2));
                book.setAutor(resultSet.getString(3));
                book.setGen(resultSet.getString(4));
                book.setQuantity(resultSet.getInt(5));
                book.setPrice(resultSet.getInt(6));

                books.add(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return books;
    }

    @Override
    public List<Book> findByTitleAndGenre(String title, String genre){

        Connection connection = jdbConnectionWrapper.getConnection();
        List<Book> books = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM book WHERE title=? AND genre=?");
            preparedStatement.setString(1, title);
            preparedStatement.setString(2, genre);


            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                Book book = new Book();

                book.setId(resultSet.getInt(1));
                book.setTitle(resultSet.getString(2));
                book.setAutor(resultSet.getString(3));
                book.setGen(resultSet.getString(4));
                book.setQuantity(resultSet.getInt(5));
                book.setPrice(resultSet.getInt(6));

                books.add(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return books;
    }

    @Override
    public List<Book> findByAuthorAndGenre(String author, String genre){

        Connection connection = jdbConnectionWrapper.getConnection();
        List<Book> books = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM book WHERE author=? AND genre=?");
            preparedStatement.setString(1, author);
            preparedStatement.setString(2, genre);


            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                Book book = new Book();

                book.setId(resultSet.getInt(1));
                book.setTitle(resultSet.getString(2));
                book.setAutor(resultSet.getString(3));
                book.setGen(resultSet.getString(4));
                book.setQuantity(resultSet.getInt(5));
                book.setPrice(resultSet.getInt(6));

                books.add(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return books;
    }
}
