package repository;

import model.*;
import java.util.List;
import java.util.*;

public interface BookRepository {

    Book create(Book book);

    Book update(Book book);


    //vinde
    boolean delete(int id);

    List<Book> view();

    List<Book> findByTitle(String title);
    List<Book> findByAuthor(String autor);
    List<Book> finfByGenre(String genre);

     Book findById(int id);

     List<Book> findByTitleAndAuthor(String title, String author);
    List<Book> findByTitleAndGenre(String title, String genre);
    List<Book> findByAuthorAndGenre(String author, String genre);

}
