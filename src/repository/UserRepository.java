package repository;

import java.util.List;
import model.User;

public interface UserRepository {

    List<User> findAll();

    User findById(Long id);

    User loadByUserName(String userName);

    User save(User user);

    User update(User user);

    boolean delete(Long id);

}
