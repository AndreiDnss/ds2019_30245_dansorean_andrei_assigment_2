package report;

public class ReportFactory {

    public static ReportInterface getReport(String type){
        if(type == null){
            return null;
        }
        if(type.equalsIgnoreCase("PDF")){
            return new PDFReport();

        } else if(type.equalsIgnoreCase("CSV")){
            return new CSVReport();
        }

        return null;
    }

}
