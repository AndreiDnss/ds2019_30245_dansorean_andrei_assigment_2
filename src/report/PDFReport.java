package report;

import java.io.FileOutputStream;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

public class PDFReport implements ReportInterface {


    public PDFReport() {}



    @Override
    public void gen(String info) {
        Document doc=new Document();
        try {

            PdfWriter.getInstance(doc, new FileOutputStream("rPDF.pdf"));
            doc.open();

            Paragraph p=new Paragraph();

            p.add(info);
            doc.add(p);
            doc.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
