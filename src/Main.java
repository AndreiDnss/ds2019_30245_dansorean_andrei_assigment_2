
import repository.BookRepository;
import repository.UserRepository;
import repository.impl.*;
import service.*;
import view.*;


public class Main {



    private static final String SCHEMA_NAME = "book";

    private static ContextHolder contextHolder;

    private static UserService userService;
    private static BookService bookService;



    public static void main(String[] args) {

        //init db connection
        JDBConnectionWrapper jdbConnectionWrapper = new JDBConnectionWrapper(SCHEMA_NAME);

        //init repositories
        BookRepository bookRepository = new BookRepositoryImpl(jdbConnectionWrapper);
        UserRepository userRepository = new UserRepositoryImpl(jdbConnectionWrapper);

        //service
        contextHolder = new ContextHolderImpl();
        userService = new UserServiceImpl(userRepository, contextHolder);
        bookService= new BookServiceImpl(bookRepository);

        LogIn logIn = new LogIn(userService, bookService);

    }
}
