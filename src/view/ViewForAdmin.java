package view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import model.*;
import report.PDFReport;
import report.ReportFactory;
import report.ReportInterface;
import repository.*;
import service.*;
import view.tables.*;

public class ViewForAdmin extends JFrame{

    private JTextField textField1;
    private JButton updateUserButton;
    private JButton addUserButton;
    private JButton viewUserButton;
    private JButton deleteUserButton;
    private JTextField addUserTextField;
    private JTextField updateUserTextField;
    private JTextField deleteUserTextField;
    private JButton addBookButton;
    private JButton updateBookButton;
    private JButton deleteBookButton;
    private JButton viewBookButton;
    private JTextField addBookTextField;
    private JTextField updateBookTextField;
    private JTextField deleteBookTextField;
    private JTable table1;
    private JTable table2;
    private JButton CSVButton;
    private JButton PDFButton;
    private JPanel panel1;
    private JFrame log;


    private static User user;
    private static Book book;

    private  static UserService userService;
    private static BookService bookService;

    private static UserRepository userRepository;
    private static BookRepository bookRepository;

    private static ReportFactory r=new ReportFactory();
    private static ReportInterface p;

   // private static ReportFactory report;


    public ViewForAdmin(UserService userService, BookService bookService) throws HeadlessException{

        this.userService=userService;
        this.bookService=bookService;

        log = new JFrame("Book Data Base");
        log.setContentPane(panel1);
        log.setSize(1300, 600);
        log.setLocationRelativeTo(null);
        log.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        log.setVisible(true);


       viewUserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<User> employees = new ArrayList<User>();
                employees = userService.findAll();
                UserTable userTable= new UserTable(employees);
                table1.setModel(userTable);

            }
        });

        addUserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (addUserTextField.getText().equals(""))
                    JOptionPane.showMessageDialog(null, "Introduceti date in campul corespunzator!!");

                else {

                    String s = addUserTextField.getText();
                    String[] result = s.split(",");

                    user = userService.addUser(result[0], result[1], UserRole.valueOf(result[2]));
                }
            }
        });



        updateUserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (updateUserTextField.getText().equals(""))
                    JOptionPane.showMessageDialog(null, "Introduceti date in campul corespunzator!!");
                else {
                    String s = updateUserTextField.getText();
                    String[] result = s.split(",");

                    user = userService.updateUser(Long.parseLong(result[0]), result[1], result[2], UserRole.valueOf(result[3]));
                }
            }
        });

        deleteUserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(deleteUserTextField.getText().equals(""))
                    JOptionPane.showMessageDialog(null, "Introduceti date in campul corespunzator!!");
                else
                    userService.delete(Long.parseLong(deleteUserTextField.getText()));
            }
        });



        //-----------------BOOKS---------------

        viewBookButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<Book> c = new ArrayList<>();
                c = bookService.findAll();
                BookTable cTable= new BookTable(c);
                table2.setModel(cTable);
            }
        });

        PDFButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<Book> c = new ArrayList<>();
                c = bookService.findAll();
                p=r.getReport("PDF");
                String s=null;
                for(Book b: c){
                    if(b.getQuantity()<1) {


                        s=s+( b.getTitle()+"," + b.getAutor()+"," + b.getGen()+"," + String.valueOf(b.getQuantity())+"," + String.valueOf(b.getPrice())+","+"\n");
                        // p.gen(s+"\n");

                    }
                    p.gen(s+"\n");
                }
            }
        });

        CSVButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<Book> c = new ArrayList<>();
                c = bookService.findAll();
                p=r.getReport("CSV");
                String s=null;
                for(Book b: c){
                    if(b.getQuantity()<1) {


                        s=s+( b.getTitle()+"," + b.getAutor()+"," + b.getGen()+"," + String.valueOf(b.getQuantity())+"," + String.valueOf(b.getPrice())+","+"\n");
                        // p.gen(s+"\n");

                    }
                    p.gen(s+"\n");
                }
            }
        });

        addBookButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (addBookTextField.getText().equals(""))
                    JOptionPane.showMessageDialog(null, "Introduceti date in campul corespunzator!!");
                else {

                    String s = addBookTextField.getText();
                    String[] result = s.split(",");

                    book = bookService.addBook(Integer.parseInt(result[0]), result[1], result[2], result[3], Integer.parseInt(result[4]),Integer.parseInt(result[5]));
                }
            }
        });

        updateBookButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (updateBookTextField.getText().equals(""))
                    JOptionPane.showMessageDialog(null, "Introduceti date in campul corespunzator!!");
                else {

                    String s = updateBookTextField.getText();
                    String[] result = s.split(",");
                    book = bookService.updateBook(Integer.parseInt(result[0]), result[1], result[2], result[3], Integer.parseInt(result[4]),Integer.parseInt(result[5]));

                }
            }
        });

        deleteBookButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(deleteBookTextField.getText().equals(""))
                    JOptionPane.showMessageDialog(null, "Introduceti date in campul corespunzator!!");
                else
                    bookService.delete(Integer.parseInt(deleteBookTextField.getText()));
            }
        });


    }

    public int getId(){
        String s = addUserTextField.getText();
        String[] result = s.split(",");
        return Integer.parseInt(result[0]);
    }

    public String getUsername(){
        String s = addUserTextField.getText();
        String[] result = s.split(",");
        return result[1];
    }

    public String getPass(){
        String s = addUserTextField.getText();
        String[] result = s.split(",");
        return result[2];
    }

    public UserRole getRole(){
        String s = addUserTextField.getText();
        String[] result = s.split(",");
        return  UserRole.valueOf(result[3]);
    }

    public void addAddUser(ActionListener listener) {
        addUserButton.addActionListener(listener);
    }

    public JFrame getLog(){

        return this.log;
    }

}
