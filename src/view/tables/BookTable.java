package view.tables;

import model.*;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class BookTable extends AbstractTableModel {

    private static final int idCol = 0;
    private static final int titleCol = 1;
    private static final int authorCol = 2;
    private static final int genreCol = 3;
    private static final int quantityCol = 4;
    private static final int priceCol = 5;

    private String[] columnNames = {"id","title","author","genre", "quantity", "price"};
    private List<Book> books;

    public BookTable(List<Book> b){
        this.books = b;
    }

    @Override
    public int getRowCount() {
        return books.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    public String getColumnName (int col) { return columnNames[col];}

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        Book temp = books.get(rowIndex);
        switch (columnIndex){
            case idCol:
                return temp.getId();
            case titleCol:
                return temp.getTitle();
            case authorCol:
                return temp.getAutor();
            case genreCol:
                return temp.getGen();
            case quantityCol:
                return temp.getQuantity();
            case priceCol:
                return temp.getPrice();
            default:
                return idCol;
        }
    }

    public Class <? extends Object > getColumnClass(int c) { return getValueAt(0,c).getClass();}
}
