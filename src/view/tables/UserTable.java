package view.tables;

import model.User;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class UserTable extends AbstractTableModel {

    private static final int idCol = 0;
    private static final int usernameCol = 1;
    private static final int passwordCol = 2;
    private static final int roleCol = 3;

    private String[] columnNames = {"id","user_name","password","role"};
    private List<User> users;

    public UserTable(List<User> u){
        this.users = u;
    }
    @Override
    public int getRowCount() {
        return users.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    public String getColumnName (int col) { return columnNames[col];}

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        User temp = users.get(rowIndex);
        switch (columnIndex){
            case idCol:
                return temp.getId();
            case usernameCol:
                return temp.getUserName();
            case passwordCol:
                return temp.getPassword();
            case roleCol:
                return temp.getRole();
            default:
                return idCol;
        }
    }

    public Class <? extends Object > getColumnClass(int c) { return getValueAt(0,c).getClass();}
}
