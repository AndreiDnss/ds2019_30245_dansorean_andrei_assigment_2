package view;

import controller.AdminController;
import model.User;
import service.*;

import javax.swing.*;
import javax.swing.text.View;
import java.awt.event.ActionEvent;
import view.*;
import java.awt.event.ActionListener;

public class LogIn {
    private JTextField username;
    private JRadioButton administratorRadioButton;
    private JButton logInButton;
    private JPanel mainPannel;
    private JPasswordField passwordField1;
    private JFrame log;
    private boolean admin = false;

    private  UserService userService;
    private BookService bookService;
    private ViewForAdmin viewForAdmin;

    public LogIn(UserService userService, BookService bookService) {

        this.userService = userService;
        this.bookService=bookService;

        log = new JFrame("Log in");
        log.setContentPane(mainPannel);
        log.setSize(300, 200);
        log.setLocationRelativeTo(null);
        log.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        log.setVisible(true);

        logInButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(administratorRadioButton.isSelected() == true ){
                    admin = true;
                }
                else admin = false;
                if (admin == true) {
                    if(username.getText().equals("") || passwordField1.getPassword().length == 0){
                        JOptionPane.showMessageDialog(null,"Field empty !");
                    }
                    else {
                        // --------------------ADMIN --------------
                        String usernameEntered = username.getText();
                        String passwordEntered = new String (passwordField1.getPassword());
                        if(usernameEntered.equals("biancatusa") && passwordEntered.equals("bia")){
                            log.dispose();

                            AdminController adm=new AdminController(new ViewForAdmin(userService,bookService),userService,bookService);
                            //ViewForAdmin employeesDataBase = new ViewForAdmin(userService,bookService );
                        }
                        else{
                            JOptionPane.showMessageDialog(null,"Admin info incorrect !");
                        }
                    }
                } else {
                    if(username.getText().equals("") || passwordField1.getPassword().length == 0){
                        JOptionPane.showMessageDialog(null,"Field empty !");
                    }
                    else {
                        // ----------------USER -------------------
                        String usernameEntered = username.getText();
                        String passwordEntered = new String (passwordField1.getPassword());
                        User currentUser = userService.login(usernameEntered,passwordEntered);
                        if( currentUser != null){
                            log.dispose();
                            ViewForUser v = new ViewForUser(usernameEntered,bookService);
                        }else{
                            JOptionPane.showMessageDialog(null,"Wrong username or password ! Check console !");
                        }

                    }
                }


            }
        });
    }
}