package view;

import model.Book;
import model.User;
import repository.BookRepository;
import repository.UserRepository;
import service.BookService;
import service.UserService;
import view.tables.BookTable;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class ViewForUser {
    private JButton titleGenreButton;
    private JButton titleAuthorButton;
    private JButton genreButton;
    private JButton authorButton;
    private JButton titleButton;
    private JButton authorGenreButton;
    private JTextField titleTextField;
    private JTextField authorTextField;
    private JTextField genreTextField;
    private JTable table1;
    private JButton sellButton;
    private JTextField sellTextField;
    private JPanel p2;
    private JFrame log;


    private static Book book;

    private static BookService bookService;

    private static BookRepository bookRepository;

    private String username;

    public ViewForUser(String username, BookService bookService) {

        this.username=username;
        this.bookService = bookService;

        log = new JFrame("Bank Data Base");
        log.setContentPane(p2);
        log.setSize(1300, 600);
        log.setLocationRelativeTo(null);
        log.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        log.setVisible(true);

        titleButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<Book> b = new ArrayList<Book>();
                String s = titleTextField.getText();
                b = bookService.findByTitle(s);
                BookTable userTable= new BookTable(b);
                table1.setModel(userTable);

            }
        });



        authorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<Book> b = new ArrayList<Book>();
                String s = authorTextField.getText();
                b = bookService.findByAuthor(s);
                BookTable userTable= new BookTable(b);
                table1.setModel(userTable);

            }
        });

        genreButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<Book> b = new ArrayList<Book>();
                String s = genreTextField.getText();
                b = bookService.findByGenre(s);
                BookTable userTable= new BookTable(b);
                table1.setModel(userTable);

            }
        });

        titleAuthorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<Book> b = new ArrayList<Book>();
                String s1 = titleTextField.getText();
                String s2 = authorTextField.getText();
                b = bookService.find1(s1,s2);
                BookTable userTable= new BookTable(b);
                table1.setModel(userTable);

            }
        });

        titleGenreButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<Book> b = new ArrayList<Book>();
                String s1 = titleTextField.getText();
                String s2 = genreTextField.getText();
                b = bookService.find2(s1,s2);
                BookTable userTable= new BookTable(b);
                table1.setModel(userTable);

            }
        });

        authorGenreButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<Book> b = new ArrayList<Book>();
                String s1 = authorTextField.getText();
                String s2 = genreTextField.getText();
                b = bookService.find3(s1,s2);
                BookTable userTable= new BookTable(b);
                table1.setModel(userTable);

            }
        });

        sellButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (sellTextField.getText().equals(""))
                    JOptionPane.showMessageDialog(null, "Introduceti date in campul corespunzator!!");
                else {
                    String s = sellTextField.getText();
                    String[] result = s.split(",");

                    bookService.sell(Integer.parseInt(result[0]),Integer.parseInt(result[1]));
                }

            }
        });


    }
}