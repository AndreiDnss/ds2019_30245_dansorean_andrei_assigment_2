package controller;

import model.Book;
import model.User;
import model.UserRole;
import repository.BookRepository;
import repository.UserRepository;
import service.BookService;
import service.UserService;
import view.ViewForAdmin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.Normalizer;

public class AdminController {

    private final ViewForAdmin viewForAdmin;
    private  final UserService userService;
    private final BookService bookService;


    public AdminController(ViewForAdmin viewForAdmin,UserService userService, BookService bookService) {
        this.viewForAdmin = viewForAdmin;
        this.userService=userService;
        this.bookService=bookService;

        viewForAdmin.addAddUser(new AddUser());
    }


    private class AddUser implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String username=viewForAdmin.getUsername();
            String pass=viewForAdmin.getPass();
            UserRole role=viewForAdmin.getRole();

            userService.addUser( username, pass,role);
           // refreshSelectedShoppingBasket();
        }
    }
}
