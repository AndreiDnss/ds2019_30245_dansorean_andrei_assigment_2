package model.builder;


import model.*;

public class UserBuilder {

    private Long id;
    //initialized with default necesarry data
    private String userName = "defaultUsername";
    private String password = "defaultPassword";
    private UserRole role = UserRole.USER;

    public UserBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public UserBuilder withUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public UserBuilder withPassword(String password) {
        this.password = password;
        return this;
    }

    public UserBuilder withRole(UserRole role) {
        this.role = role;
        return this;
    }

    public User build() {
        User user = new User();
        user.setId(id);
        user.setUserName(userName);
        user.setPassword(password);
        user.setRole(role);

        return user;
    }
}
