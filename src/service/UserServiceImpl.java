package service;
import model.*;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import repository.*;
import model.*;
import repository.impl.*;
import repository.impl.BookRepositoryImpl;

public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = Logger.getLogger(BookRepositoryImpl.class.getName());

    private final UserRepository userRepository;
    private final ContextHolder contextHolder;
    private static User user;

    public UserServiceImpl(UserRepository userRepository,
                           ContextHolder contextHolder) {
        this.userRepository = userRepository;
        this.contextHolder = contextHolder;
    }

    @Override
    public User login(String userName, String password) {
        User user = userRepository.loadByUserName(userName);
        if(user != null) {
            if(user.getPassword().equals(password)) {
                contextHolder.setLoggedInUser(user);
                return user;
            } else {
                LOGGER.warning("Wrong password for user " + userName);
            }
        } else {
            LOGGER.warning("User with username: " + userName + " was not found");
        }
        return null;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User addUser( String username, String password, UserRole role) {
        User user= new User();

        //user.setId(userId);
        user.setUserName(username);
        user.setPassword(password);
        user.setRole(role);

        return userRepository.save(user);
    }

    @Override
    public User updateUser(Long userId, String username, String password, UserRole role) {
       // User us=userRepository.findById(userId);

       // us.setUserName(username);
       // us.setPassword(password);
        //us.setRole(role);

        //return userRepository.update(us);

          List<User> users=userRepository.findAll();

        //users.stream().filter(u->u.getId()==(userId));

        users.stream().filter(u->u.getId()==(userId)).forEach(u->u.setUserName(username));
        users.stream().filter(u->u.getId()==(userId)).forEach(u->u.setPassword(password));
        users.stream().filter(u->u.getId()==(userId)).forEach(u->u.setRole(role));
        users.stream().filter(u->u.getId()==(userId)).forEach(u-> userRepository.update(u));

      /*for(User u:users){
           u.setUserName(username);
           u.setPassword(password);
           u.setRole(role);
           return userRepository.update(u);
       }*/
       return null;
    }



    @Override
    public void delete(Long userId) {
      //   User us=userRepository.findById(userId);
     //  List<User> users=userRepository.findAll();

      // users.stream().filter(u->u.getId().equals(userId)).forEach(users.remove(userId));
       //

        userRepository.delete(userId);
    }


}
