package service;

import java.util.List;
import model.*;

public interface BookService {

    List<Book> findAll();

    Book addBook(int id, String title, String author, String genre, int quantity, int price);

    Book updateBook(int id, String title, String author, String genre, int quantity, int price );

    public void delete(int id);

    void sell(int id, int quantity);

    List<Book> findByTitle(String title);
    List<Book> findByAuthor(String author);
    List<Book> findByGenre(String genre);

    List<Book> find1(String title, String author);
    List<Book> find2(String title, String genre);
    List<Book> find3(String author, String genre);


}
