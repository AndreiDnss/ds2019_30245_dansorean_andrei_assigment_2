package service;

import model.*;

public class ContextHolderImpl implements ContextHolder{

    private static User currentUser;

    @Override
    public User getLoggedInUser() {
        return currentUser;
    }

    @Override
    public void setLoggedInUser(User user) {
        currentUser = user;
    }


}
