package service;

import model.*;

import java.util.List;

public interface UserService {

    User login(String userName, String password);

    List<User> findAll();

    User addUser( String username, String password, UserRole role );

    User updateUser(Long userId, String username, String password, UserRole role );

    public void delete(Long userId);
}
