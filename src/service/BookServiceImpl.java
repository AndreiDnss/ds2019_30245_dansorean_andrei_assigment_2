package service;

import model.Book;
import repository.*;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;

public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;

    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }


    @Override
    public List<Book> findAll() {
        return bookRepository.view();
    }

    @Override
    public Book addBook(int id, String title, String author, String genre, int quantity, int price) {
        Book b=new Book();

        b.setId(id);
        b.setTitle(title);
        b.setAutor(author);
        b.setGen(genre);
        b.setQuantity(quantity);
        b.setPrice(price);

        return bookRepository.create(b);

    }

    @Override
    public Book updateBook(int id, String title, String author, String genre, int quantity, int price) {
        Book b=bookRepository.findById(id);

        b.setTitle(title);
        b.setAutor(author);
        b.setGen(genre);
        b.setQuantity(quantity);
        b.setPrice(price);

        return bookRepository.update(b);
    }

    @Override
    public void delete(int id) {
       bookRepository.delete(id);
    }

    @Override
    public void sell(int id, int quantity) {
          //  Book b=bookRepository.findById(id);

         //   int total=b.getQuantity();
        //    total=total-quantity;

           // b.setQuantity(total);

           // bookRepository.update(b);

        List<Book> books=bookRepository.view();

        books.stream().filter(b->b.getId()==id).forEach(b->b.setQuantity(b.getQuantity()-quantity));
        books.stream().filter(b->b.getId()==id).forEach(b-> bookRepository.update(b));


    }

    @Override
    public List<Book> findByTitle(String title) {

     //   List<Book> b1=bookRepository.view();
      //  List<Books> b2= b1.stream().filter(b->b.getTitle()==title).collect(Collectors.toList());

        return bookRepository.findByTitle(title);
    }

    @Override
    public List<Book> findByAuthor(String author) {
        return bookRepository.findByAuthor(author);
    }

    @Override
    public List<Book> findByGenre(String genre) {
        return bookRepository.finfByGenre(genre);
    }

    @Override
    public List<Book> find1(String title, String author) {
        return bookRepository.findByTitleAndAuthor(title,author);
    }

    @Override
    public List<Book> find2(String title, String genre) {
        return bookRepository.findByTitleAndGenre(title,genre);
    }

    @Override
    public List<Book> find3(String author, String genre) {
        return bookRepository.findByAuthorAndGenre(author,genre);
    }
}
