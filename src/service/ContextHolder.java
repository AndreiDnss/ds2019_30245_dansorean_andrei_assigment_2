package service;

import model.*;

public interface ContextHolder {

    User getLoggedInUser();

    void setLoggedInUser(User user);

}

